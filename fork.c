#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv [] ) {
  int pid;
  /* fork another process */
  printf ("1. Start the program. Before Fork \n") ;
  pid = fork();
  printf ("2. After the fork. This is pid's value %d\n",pid) ;

  if (pid < 0) { /* error occurred */
    fprintf (stderr , "Fork Failed") ;
    exit (-1) ;
  } else if (pid == 0) { /* This is child process */
    printf ("3. Child Process: PID should be 0 = %d\n", pid) ;
    execlp("/bin/ls" , "ls" ,NULL) ;
    //execlp("/bin/vi" , "vi" ,NULL) ;
    printf ("4. Child Process: I have finished"); 
  } else { /* This is parent process */
    /* parent will wait for the child to complete */
    printf( "5. This is Parent: Before waiting. PID of the child that has been created: %d.\n", pid );
    wait (NULL) ;
    printf ("6. This is Parent: After Waiting when Child Completed \n") ;
    exit (0) ;
  }
}
