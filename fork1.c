/*-----------------------------------------------------------------------------
 * program fork
 * Original: Silberschatz et al., Chapter 4
 *  - getpid() and getppid() calls added
 *  - child sleeps 1 second at the beginning so that parent code
 *    runs first
 *  - parent sleeps after wait returns to make it appear in ps
 *
 * compile:  gcc -o fork1 fork1.c
 * run:     ./fork1
 *---------------------------------------------------------------------------*/
 #include <stdio.h>
 #include <sys/types.h>
 #include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
 {
 	int pid;
	printf("The original process, pid is %d\n", getpid());
	pid = fork();
	/* error checking */
	if (pid < 0) {
		perror("at fork");
		_exit(-1);
	}
	if (pid == 0) {	/* child process */
		sleep(1);
		printf("Child: My pid is %d and my parent pid is %d\n"
		       "In five seconds I will be executing: /bin/ls -l\n", 
			getpid(), getppid());
		sleep(5);		
		execlp("/bin/ls", "ls","-l", NULL);
		printf("Should not be reached\n");
	} else {       /* parent process */
		printf("Parent: My pid is %d and my parent pid is %d\n"
		       "My child has the pid %d\n"
		       "I am waiting for my child\n", 
			getpid(), getppid(), pid);
		wait(NULL);
	}
		printf("Parent: Child terminated\n");
		printf("Parent: exiting\n");
		_exit(0);
	_exit(0);
 }
