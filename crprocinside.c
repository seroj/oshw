#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

void ChildProcess();

int main() {
	int pid, cid, r;
	pid = getpid();
	r = fork(); //create a new process

	if (r == 0) { //r=0 -> it is child

		cid = getpid(); //get process ID
		printf("I am the child with cid = %d of pid = %d \n", cid, pid);
		ChildProcess();
		exit(0);
	} else {
		printf("Parent waiting for the child...\n");
		wait(NULL);
		printf("Child finished, parent quitting too!");
	}
}

void ChildProcess() {
	int i;

	for (i = 0; i < 5; i++) {
		printf("%d ..\n", i);
		sleep(1);
	}
}


