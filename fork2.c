#include <stdio.h>
 #include <sys/types.h>
 #include <unistd.h>
#include <time.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
 {
 	int pid1, pid2;
	int napChild = 5, napGrandChild = 10;
	time_t t0 = time(NULL);
	printf("Parent process starting, My pid is %d\n", getpid());
	/*
	 * parent: create child
	 */
	pid1 = fork();
	if (pid1 < 0) {		/* error checking */
		perror("at fork");
		_exit(-1);
	}
	if (pid1 == 0) {	/* child process */
		printf("At %3d:Child process: my pid is %d and my parent pid is %d\n"
		       "I am creating my own child process\n", 
			time(NULL) - t0, getpid(), getppid());
		/*
		 * create grand child
		 */
		pid2 = fork();
		if (pid2 < 0) {		/* error checking */
			perror("at fork");
			_exit(-1);
		}
		if (pid2 == 0) {	/* grand child process */
			sleep(1);
			printf("At %3d:Grand child process: my pid is %d and my parent pid is %d\n"
			       "I am sleeping for %d seconds\n",
				time(NULL) - t0, getpid(), getppid(), napGrandChild);
			sleep(napGrandChild);
			printf("At %3d:Grand child process:my pid is %d and my parent pid is %d\n"
				"I am terminating\n", 
				time(NULL) - t0, getpid(), getppid());
		} else {       /* first child process, cont: sleep less than grand child */
			printf("At %3d:Child process: My child's pid is %d\n"
				"Im sleeping for %d seconds\n", time(NULL) - t0, pid2, napChild);
			sleep(napChild);
			printf("At %3d:Child process:I am terminating\n", time(NULL) - t0);
		}		
	} else {       /* parent process, cont: wait for child */	
		printf("At %3d:Parent process: my pid is %d and my parent pid is %d\n"
		       "My child has the pid %d\n"
		       "I am waiting for my child\n", 
			time(NULL) - t0, getpid(), getppid(), pid1);
		wait(NULL);
		printf("At %3d:Parent process:child terminating, sleeping for 10 secs\n", 
                        time(NULL) - t0);
		sleep(10);
		
		printf("At %3d:Parent process: executing /bin/ps l\n", 
                        time(NULL) - t0);
		execlp("/bin/ps", "ps", "l", NULL);
	}
	_exit(0);
 }
