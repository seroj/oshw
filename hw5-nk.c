//
// Created by Seroj on 3/12/16.
//
// This one does not kill the parent and so are no orphan(zombie) processes
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    int pid1, pid2, pid3;
    int napChild = 10, napGrandChild = 15, nagGrandGrandChild = 20;
    time_t t0 = time(NULL);
    printf("Parent process starting, My pid is %d\n", getpid());

    pid1 = fork();
    if (pid1 < 0) {
        perror("at fork");
        _exit(-1);
    }
    if (pid1 == 0) {
        printf("At %3d:Child process: my pid is %d and my parent pid is %d\n"
                       "I am creating my own child process\n",
               time(NULL) - t0, getpid(), getppid());

        pid2 = fork();
        if (pid2 < 0) {
            perror("at fork");
            _exit(-1);
        }
        if (pid2 == 0) {
            sleep(5);
            printf("At %3d:Grand child process: my pid is %d and my parent pid is %d\n"
                           "I am sleeping for %d seconds\n",
                   time(NULL) - t0, getpid(), getppid(), napGrandChild);

            pid3 = fork();
            if (pid3 < 0) {
                perror("at fork");
                _exit(-1);
            }
            if (pid3 == 0) {
                sleep(5);
                printf("At %3d:Grand-Grand child process: my pid is %d and my parent pid is %d\n"
                               "I am sleeping for %d seconds after running the INTERNAL func() \n",
                       time(NULL) - t0, getpid(), getppid(), nagGrandGrandChild);
                myInternalFunction();

                sleep(nagGrandGrandChild);
                printf("At %3d:Grand child process:my pid is %d and my parent pid is %d\n"
                               "I am terminating\n",
                       time(NULL) - t0, getpid(), getppid());
            } else {
                printf("At %3d:Child process: My child's pid is %d\n"
                               "Im sleeping for %d seconds\n", time(NULL) - t0, pid2, napChild);
                sleep(napChild);
                printf("At %3d:Child process:I am terminating\n", time(NULL) - t0);
            }

            sleep(napGrandChild);
            printf("At %3d:Grand child process:my pid is %d and my parent pid is %d\n"
                           "I am terminating\n",
                   time(NULL) - t0, getpid(), getppid());

        } else {
            printf("At %3d:Child process: My child's pid is %d\n"
                           "Im sleeping for %d seconds\n", time(NULL) - t0, pid2, napChild);
            sleep(napChild);
            printf("At %3d:Child process:I am terminating\n", time(NULL) - t0);
        }
    } else {
        printf("At %3d:Parent process: my pid is %d and my parent pid is %d\n"
                       "My child has the pid %d\n"
                       "I am waiting for my child\n",
               time(NULL) - t0, getpid(), getppid(), pid1);
        wait(NULL);
        printf("At %3d:Parent process:child terminating, sleeping for 10 secs\n",
               time(NULL) - t0);
        sleep(10);

        printf("At %3d:Parent process: executing /bin/ps l\n",
               time(NULL) - t0);
        execlp("/bin/ps", "ps", "l", NULL);
    }
    _exit(0);
}

int myInternalFunction() {
    printf("\n/-----------------------/\nI AM THE INTERNAL ONE, DO NOT CONFUSE ME ! \n/-----------------------/\n\n");
}